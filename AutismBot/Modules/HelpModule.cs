﻿using Discord.Commands;
using System.Threading.Tasks;

public class HelpModule : ModuleBase<SocketCommandContext> {

    private readonly CommandService _service;

    //init HelpModule
    public HelpModule(CommandService service) {
        _service = service;
    }

    [Command("help")]
    [Summary("It helps u n stuff my dude")]
    public async Task HelpAsync() {
        await ReplyAsync("PMed you a list of commands!");
        string message = "```";
        foreach (var module in _service.Modules) {
            message += "-------------------" + module.Name + "-------------------\n";
            //await Discord.UserExtensions.SendMessageAsync(Context.User, "-------------------" + module.Name + "-------------------");
            foreach (var cmd in module.Commands) {
                message += cmd.Name + " : " + cmd.Summary + "\n";
                //await Discord.UserExtensions.SendMessageAsync(Context.User,  cmd.Name + " : " + cmd.Summary);
            }
            message += "\n";
        }
        message += "```";
        await Discord.UserExtensions.SendMessageAsync(Context.User, message);
    }
}