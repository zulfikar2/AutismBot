﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using System;
using System.Threading.Tasks;

public class AdministratorModule : ModuleBase<SocketCommandContext> {

    [Group("set")]
    public class setter : ModuleBase<SocketCommandContext> {

        // ~set nick [nickname] -> set bot nickname
        [Command("nick")]
        [Summary("Sets nickname!")]
        public async Task NicknameAsync(string nick) {
            if (string.IsNullOrWhiteSpace(nick))
                return;
            await Context.Guild.CurrentUser.ModifyAsync(x => {
                x.Nickname = nick;
            });

            Console.WriteLine(Context.User + " used command [" + Context.Message + "] at " + System.DateTime.Now + " in chat : " + Context.Channel.Name);
            await ReplyAsync("Nickname changed to : " + nick);
        }
    }

    [Group("info")]
    public class info : ModuleBase<SocketCommandContext> {
        // ~userinfo --> foxbot#0282
        // ~userinfo @Khionu --> Khionu#8708
        // ~userinfo Khionu#8708 --> Khionu#8708
        // ~userinfo Khionu --> Khionu#8708
        // ~userinfo 96642168176807936 --> Khionu#8708
        // ~whois 96642168176807936 --> Khionu#8708
        [Command("")]
        [Summary("Returns info about the current user, or the user parameter, if one passed.")]
        [Alias("user", "whois")]
        public async Task UserInfoAsync([Summary("The (optional) user to get info for")] SocketUser user = null) {
            var userInfo = user ?? Context.Client.CurrentUser;
            await ReplyAsync($"{userInfo.Username}#{userInfo.Discriminator}");
            await ReplyAsync(userInfo.GetAvatarUrl() ?? "http://ravegames.net/ow_userfiles/themes/theme_image_22.jpg");
        }
    }

    /*// ~invite
    [Command("invite")]
    [Summary("Invites bot to another server")]
    public async Task InviteAsync(ulong id) {
        var RequestedGuild = Context.Client.GetGuild(id);
        IInviteMetadata GuildDefault =
            await (RequestedGuild.GetChannel(RequestedGuild.DefaultChannel.Id) as IGuildChannel)
                .CreateInviteAsync();
        await Context.Channel.SendMessageAsync("Invite link: " + GuildDefault.Url);
    }*/


}