﻿using Discord.Commands;
using Discord.WebSocket;
using System;
using System.Threading.Tasks;

public class EconomyModule : ModuleBase<SocketCommandContext> {

    [Group("set")]
    public class setter : ModuleBase<SocketCommandContext> {

        // ~set autismbucks -> sets user's money
        [Command("autismbucks")]
        [Summary("sets user's money!")]
        public async Task SetAsync(int num, SocketUser user = null) {
            var currTime = DateTime.Now;
            //no user specified. So it sets self
            if (user == null) {
                Console.WriteLine(currTime.ToString("HH:mm:ss") + " " + Context.User + "ID : " + Context.User.Id + " used command !set autismbucks");
                await ReplyAsync(Context.User + " now has " + num + " autism bucks!");
            }
            else {
                Console.WriteLine(currTime.ToString("HH:mm:ss") + " " + Context.User + "ID : " + Context.User.Id + " used command !set autismbucks for " + user.Username + "ID : " + user.Id);
                await ReplyAsync(user.Username + " now has " + num + " autism bucks!");
            }

        }
    }

    [Group("give")]
    public class giver : ModuleBase<SocketCommandContext> {

    }


    // ~daily
    [Command("daily")]
    [Summary("Get Daily Autism Bucks!")]
    public async Task AutismAsync() {
        Console.WriteLine(Context.User + " used command [" + Context.Message + "] at " + System.DateTime.Now + " in chat : " + Context.Channel.Name);

        string currTime = System.DateTime.Now.ToString();
        ulong userID = Context.User.Id;

        Console.WriteLine("Time : " + currTime + " - userID : " + userID);

        await ReplyAsync("REEEEEEEE!");
    }
}