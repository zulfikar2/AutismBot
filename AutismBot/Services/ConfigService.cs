﻿
using Newtonsoft.Json;
using System.Collections.Concurrent;
using System.IO;

namespace AutismBot.Services {

    public static class ConfigService {

        private static JsonSerializer JsonSerializer = new JsonSerializer();
        private static ConcurrentDictionary<string, string> _configDict = new ConcurrentDictionary<string, string>();


        public static void LoadConfig() {
            if (!File.Exists("config.json")) {
                throw new IOException("config.json file could not be found at " + Directory.GetCurrentDirectory());
            }

            using (StreamReader sr = File.OpenText("config.json"))
            using (JsonReader reader = new JsonTextReader(sr)) {
                _configDict = JsonSerializer.Deserialize<ConcurrentDictionary<string, string>>(reader);
            }
        }


        public static string getConfig(string key) {
            string result = "";
            _configDict.TryGetValue(key, out result);
            return result;
        }
    }
}
