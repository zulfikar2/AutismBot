﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.IO;

namespace AutismBot
{
    class DataLoader {

        JObject userBase;

        public DataLoader() {
            Console.WriteLine("Reading Bank file...");
            using (StreamReader reader = File.OpenText(@"c:\data.json")) {
                userBase = (JObject)JToken.ReadFrom(new JsonTextReader(reader));
                //System.Console.WriteLine(userBase);
            }
        }
    }
}
