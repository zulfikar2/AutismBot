﻿
namespace AutismBot
{
    class User {
        ulong id { get; set; }
        int bucks { get; set; }

        public User(ulong uid, int account = 0) {
            id = uid;
            bucks = account;
        }

    }
}
